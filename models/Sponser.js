var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var Sponser = new Schema({
    logo   : {type: String, required: false},
    name   : {type: String, required: true},
    stage  : {type: String, required: false},
    status : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
Sponser.plugin(uniqueValidator);
module.exports = mongoose.model('Sponser', Sponser);

