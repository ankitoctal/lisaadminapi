var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Cms = new Schema({
    page_name    : {type: String, required: true},
    page_title  : {type: String, required: true},
    slug: { type: String, slug: "page_title" },
    page_description   : {type: String, required: false},
    meta_title   : {type: String, required: false},
    meta_description   : {type: String, required: false},
    meta_keywords :{type: String, required: false},
    status    : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
Cms.plugin(uniqueValidator);
module.exports = mongoose.model('Cms', Cms);

