var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StageScore = new Schema({
    game_id  : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Game'},
    team_id : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Team'},
    score   : {type: String, required: false,},
    savedMin   : {type: String, required: false},
    stage   : {type: String, required: false},
    status  : {type: String, required: false},
    min_price  : {type: String, required: false},
    total_spent  : {type: String, required: false},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);

module.exports = mongoose.model('StageScore', StageScore);
