var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Inspector = new Schema({
    full_name    : {type: String, required: true},
    designation    : {type: String, required: false},
    bio    : {type: String, required: false},
    file    : {type: String, required: false},
    slug: { type: String, slug: "name" },
    status    : {type: String, required: false,enum : ['0', '1'], default: 1}},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Inspector', Inspector);

