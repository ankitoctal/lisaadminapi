var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Game = new Schema({
    title   : {type: String, required: true,},
    slug   : {type: String, required: false},
    description   : {type: String, required: false},
    budget  : {type: String, required: true},
    saving_amount  : {type: String, required: false},
    floor   : {type: String, required: true},
    status  : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);


module.exports = mongoose.model('Game', Game);

