var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Question = new Schema({
    title : {type: String, required: true},
    question : {type: String, required: true},
    slug: { type: String, slug: "title" },
    // building: [{ type: Schema.Types.ObjectId, ref: 'Building', required: false }],
    stage: {type: String, required: false },
    status    : {type: String, required: false,enum : ['0', '1'], default: 1}
    },
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Question', Question);