var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Contractor = new Schema({
    name    : {type: String, required: true},
    speed    : {type: String, required: true},
    cost    : {type: String, required: true},
    quality    : {type: String, required: true},
    safty    : {type: String, required: true},
    category : {type: Number, required: true},
    slug: { type: String, slug: "name" },
    status    : {type: String, required: false,enum : ['0', '1'], default: 1}},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Contractor', Contractor);

