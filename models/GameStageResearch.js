var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameStageResearch = new Schema({
    gameId  : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Game'},
    stage   : {type: String, required: true,},
    title   : {type: String, required: false,},
    content : {type: String, required: true}
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);


module.exports = mongoose.model('GameStageResearch', GameStageResearch);

