var mongoose = require('mongoose');

const teamName = new mongoose.Schema(
	{
		team_name: {  type: String, required: true, unique: true }
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);


module.exports = mongoose.model('Team', teamName);
