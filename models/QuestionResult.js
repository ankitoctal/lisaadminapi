var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var QuestionResult = new Schema({
    game_id : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Game'},
    team_id : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Team'},
    stage   : {type: String, required: false,},
    answers : {type: Object, required: false},
    status  : {type: String, required: false,enum : ['0', '1'], default: 1}
    },
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('QuestionResult', QuestionResult);