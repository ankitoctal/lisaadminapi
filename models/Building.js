var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Building = new Schema({
    name    : {type: String, required: true},
    file    : {type: String, required: true},
    description    : {type: String, required: true},
    slug: { type: String, slug: "name" },
    status    : {type: String, required: false,enum : ['0', '1'], default: 1}},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Building', Building);

