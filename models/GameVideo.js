var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var GameVideo = new Schema({
    category    : {type: String, required: true},
    title  : {type: String, required: false},
    short_description   : {type: String, required: false},
    video   : {type: String, required: false},
    status    : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
GameVideo.plugin(uniqueValidator);
module.exports = mongoose.model('GameVideo', GameVideo);

