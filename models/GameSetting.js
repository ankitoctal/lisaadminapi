var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var GameSetting = new Schema({
    game_start_date    : {type: String, required: false},
    game_end_date  : {type: String, required: true},
    status    : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
GameSetting.plugin(uniqueValidator);
module.exports = mongoose.model('GameSetting', GameSetting);

