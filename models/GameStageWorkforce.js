var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameStageWorkforce = new Schema({
    gameId  : { type: String, required: false},
    team_id : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Team'},
    concrete : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'},
    electrical : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'},
    mechanical : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'},
    plumbing : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'},
    drywall : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'},
    curtainwall : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'},
    finish : { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Contractor'}
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
        versionKey: false
    }
);


module.exports = mongoose.model('GameStageWorkforce', GameStageWorkforce);



