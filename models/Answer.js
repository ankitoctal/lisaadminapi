var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Answer = new Schema({
    answer : {type: String, required: true},
    question_id : { type: Schema.Types.ObjectId, ref: 'Question', required: false },
    price: { type:String, required:false },
    choice:{ type:String, required:false },
    status  : {type: String, required: false,enum : ['0', '1'], default: 1}
    },
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Answer', Answer);