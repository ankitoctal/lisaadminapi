var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');

var Schema = mongoose.Schema;

var PlacementAnswer = new Schema({
    team_id  : { type: Schema.Types.ObjectId, ref: 'Team', required: true },
    game_id  : { type: Schema.Types.ObjectId, ref: 'Game', required: true },
    material : { type: Schema.Types.ObjectId, ref: 'Placement', required: true },
    crane	 : { type: Schema.Types.ObjectId, ref: 'Placement', required: true },
    trailer  : { type: Schema.Types.ObjectId, ref: 'Placement', required: true },
    ingress  : { type: Schema.Types.ObjectId, ref: 'Placement', required: true },
    status   : { type: String, required: false, enum : ['0', '1'], default: 1}},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('PlacementAnswer', PlacementAnswer);

