const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const chatSchema = new Schema(
  {
    message: {
      type: String
    },
    sender: {
      type: String
    },
    user_name: {
      type: String
    },
    team_id: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

let Chat = mongoose.model("userChat", chatSchema);

module.exports = Chat;
