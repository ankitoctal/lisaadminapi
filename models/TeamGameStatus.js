var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TeamGameStatus = new Schema({
    game_id  : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Game'},
    team_id  : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Team'},
    stage   : {type: String, required: false},
    start_time : {type: String, required: false},
    remaining_time : {type: String, required: true},
    current_route : {type: String, required: false},
    dead_line  : {type: String, required: false},
    status    : {type: String, required: false,enum : ['0', '1'], default: 0},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);


module.exports = mongoose.model('TeamGameStatus', TeamGameStatus);