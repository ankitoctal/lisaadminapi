var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Placement = new Schema({
    choice    : {type: String, required: true},
    answer    : {type: String, required: true},
    category: [{ type: Schema.Types.ObjectId, ref: 'PlacementCategory', required: false }],
    slug: { type: String, slug: "choice" },
    status    : {type: String, required: false,enum : ['0', '1'], default: 1}},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Placement', Placement);

