
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameStage = new Schema({
    gameId  : { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Game'},
    stage   : {type: String, required: true,},
    title   : {type: String, required: false,},
    video   : {type: String, required: true},
    saving_amount   : {type: String, required: true},
    researchCenter   : {type: String, required: true},
    strategyDetail   : {type: String, required: true},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);


module.exports = mongoose.model('GameStage', GameStage);