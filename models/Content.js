var mongoose = require('mongoose');
slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema = mongoose.Schema;
var Content = new Schema({
    page_name    : {type: String, required: true},
    slug: { type: String, slug: "page_name" },
    start_date  : {type: String, required: false},
    end_date   : {type: String, required: false},
    main_page_title   : {type: String, required: false},
    main_page_subtitle   : {type: String, required: false},
    status    : {type: String, required: false,enum : ['0', '1'], default: 1}},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
module.exports = mongoose.model('Content', Content);

