var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var Logo = new Schema({
    logo    : {type: String, required: false},
    company  : {type: String, required: true},
    link   : {type: String, required: false},
    focus   : {type: String, required: false},
    employee   : {type: String, required: false},
    reach   : {type: String, required: false},
    status    : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
Logo.plugin(uniqueValidator);
module.exports = mongoose.model('Logo', Logo);

