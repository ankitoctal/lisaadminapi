const config = include('configs/config.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');

const GameStageResearch = include("models/GameStageResearch.js");


addStageResearch = function(req, res) { 
    var gamesatge = new GameStageResearch();        
    gamesatge.stage = req.body.stage;
    gamesatge.gameId = req.body.gameId;
    gamesatge.title = req.body.title;
    gamesatge.content = req.body.content;

    gamesatge.save(function (err) {                         
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                  
        }else {
            var response = {};
            response.status ='true';
            response.msg = 'Stage Detail added successfully.';
            return res.json(response);
        }   
                            
    });      
};

getStageResearchDetails = function(req, res) {  
    var query = GameStageResearch.find();        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

viewStageResearch = function(req, res) {    

    var query = GameStageResearch.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

editStageResearch = function(req, res) { 
    var gamesatge = {};

    gamesatge.stage = req.body.stage;
    gamesatge.gameId = req.body.gameId;
    gamesatge.title = req.body.title;
    gamesatge.content = req.body.content;


    GameStageResearch.findByIdAndUpdate(req.body._id, gamesatge, function(err) {
        if (err) {
            var response = {};
                response.status ='false';
                response.msg = 'Something went wrong.';
                return res.json(response);
        } else {
            var response = {};
            response.status ='true';
            response.msg = 'Game Stage Content updated successfully.';
            return res.json(response);
        }

    });
};


module.exports = {
    addStageResearch,
    viewStageResearch,
    editStageResearch,
    getStageResearchDetails
}

