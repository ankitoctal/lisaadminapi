const config = include('configs/config.js');
const Placement = include("models/Placement.js");
exports.add = function(req, res) { 
    var object = new Placement();        
    object.category = req.body.category;
    object.choice = req.body.choice;
    object.answer = req.body.answer;
    object.save(function (err) {                            
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                  
        }else {
            var response = {};
            response.status ='true';
            response.msg = 'Placement added successfully.';
            return res.json(response);
        }   
                            
    });      
};

exports.edit = function(req, res) { 
    Placement.findOne({_id: req.body._id}, function(err, content) {
        if(!err) {
            content.category = req.body.category;
            content.choice = req.body.choice;
            content.answer = req.body.answer;
            content.save(function(err) {
                if(err)
                {
                    var response = {};
                    response.status ='false';
                    response.msg = 'Something went wrong.';
                    return res.json(response);                
                }else {
                    var response = {};
                    response.status ='true';
                    response.msg = 'Placement updated successfully.';
                    return res.json(response);
                }   
            });
        }
    });
};

exports.getDetails = function(req, res) {   
    var query = Placement.find();   
    query.populate(['category']);
    query.sort({ updated_at: -1 });     
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};
exports.view = function(req, res) {   
    var query = Placement.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};


exports.delete = function(req, res) {
    Placement.deleteOne({ _id: req.params.id }).exec();
    var response = {};
    response.status ='true';
    response.msg = 'Placement deleted successfully.';
    return res.json(response);
};

exports.updateStatus = function(req, res) {
    var object = {};                    
    object.status = req.params.status;         
    Placement.findByIdAndUpdate(req.params.id, object , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};