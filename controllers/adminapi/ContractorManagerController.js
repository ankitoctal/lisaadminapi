const config = include('configs/config.js');
const Contractor = include("models/Contractor.js");
exports.add = function(req, res) { 
	var contractor = new Contractor();		
	contractor.name = req.body.name;
	contractor.speed = req.body.speed;
	contractor.cost = req.body.cost;
	contractor.quality = req.body.quality;
	contractor.safty = req.body.safty;
	contractor.category = req.body.category;
	contractor.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Contractor Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.getDetails = function(req, res) {	
   	var query = Contractor.find();   
   	query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view = function(req, res) {	
   	var query = Contractor.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.edit = function(req, res) { 
    Contractor.findOne({_id: req.body._id}, function(err, content) {
    if(!err) {
        content.name = req.body.name;
        content.speed = req.body.speed;
		content.cost = req.body.cost;
		content.quality = req.body.quality;
		content.safty = req.body.safty;
		content.category = req.body.category;
        content.save(function(err) {
            if(err)
                {
                    var response = {};
		            response.status ='false';
		            response.msg = 'Something went wrong.';
		            return res.json(response);                
                }else {
                    var response = {};
		            response.status ='true';
		            response.msg = 'Contractor updated successfully.';
		            return res.json(response);
                }   
        });
	}
	});
};

exports.delete = function(req, res) {
    Contractor.deleteOne({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Contractor deleted successfully.';
	return res.json(response);
};

exports.updateStatus = function(req, res) { console.log(req.params.id); console.log(req.params.status);
    var contractor = {};                    
    contractor.status = req.params.status;         
    Contractor.findByIdAndUpdate(req.params.id, contractor , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};