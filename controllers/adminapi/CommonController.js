const config = include('configs/config.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');
const User = include("models/User.js");
const Email = include("models/Email.js");
const GameSetting = include("models/GameSetting.js");
const GameVideo = include("models/GameVideo.js");

function sendMail(params) {
    var query = Email.findOne({'slug': params.slug });
    query.exec(function(err, result) {
        var message = result.body;
        params.params.site_url = config.site_url;
        params.params.admin_mail = config.admin_mail;
        params.params.site_name = config.site_name;

        for (prop in params.params) {
            var val = params.params[prop];
            //message = message.replace("{{"+prop+"}}",val);	
            message = message.replace(new RegExp("{{" + prop + "}}", 'gi'), val);
        }

        var smtpTransport = nodemailer.createTransport({
            host: config.senderHost,
            port: config.senderPort,
            secure: false, // true for 465, false for other ports
            auth: {
                user: config.senderUsername,
                pass: config.senderPassword
            }
        });

        var mail = {
            from: config.senderFrom,
            to: params.to,
            subject: result.subject,
            html: message
        }
        smtpTransport.sendMail(mail, function(error, response) {
             smtpTransport.close();
            if (error) {
                return true; 
            } else {
                return false ; 
            }

           
        });

    });
}

exports.sendMail = sendMail;

exports.testmail = function(req, res) {
    var params = {};
    params.slug = 'user_register';
    params.to = 'ankit.pandey@octalinfosolution.com';
    params.subject = 'Send Email Using Node.js';
    params.params = {};
    params.params.name = 'Ankit';
    sendMail(params);

    var response = {};
    response.status = 'success';
    response.msg = 'mail sent successfully';
    return res.json(response);
};

exports.gameSetting = function(req, res) {   
    var query = GameSetting.findOne();        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

exports.saveGameSetting = function(req, res) {
    var gameSetting = {};          
    gameSetting.game_start_date =req.body.game_start_date;   
    gameSetting.game_end_date =req.body.game_end_date; 
    GameSetting.findOneAndUpdate( req.body._id, gameSetting,{new: true,upsert: true}, function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Game Setting Updated Successfully';
        return res.json(response);
    });     
};

exports.gameVideos = function(req, res) {   
    var query = GameVideo.find();        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

exports.addGameVideos = function(req, res) { 
    var gameVideo = new GameVideo();      
    gameVideo.category = req.body.category;
    gameVideo.title = req.body.title;
    gameVideo.video = req.body.video;
    gameVideo.short_description = req.body.short_description;

    gameVideo.save(function (err) {                          
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                  
        }else {
            var response = {};
            response.status ='true';
            response.msg = 'Game video added successfully.';
            return res.json(response);
        }   
                            
    });      
};

exports.deleteGameVideo = function(req, res) {
    GameVideo.remove({ _id: req.params.user_id }).exec();
    var response = {};
    response.status ='true';
    response.msg = 'Game Video deleted successfully.';
    return res.json(response);
};

exports.updateStatusGameVideo = function(req, res) {
    var gameVideo = {};                    
    gameVideo.status = req.params.status;         
    GameVideo.findByIdAndUpdate( req.params.id, gameVideo , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};

exports.viewGameVideo = function(req, res) { 
    var query = GameVideo.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

exports.editGameVideo = function(req, res) { 
    var gameVideo = {};
    gameVideo.title = req.body.title;
    gameVideo.video = req.body.video; 
    gameVideo.short_description = req.body.short_description;
    gameVideo.category = req.body.category;
    GameVideo.findByIdAndUpdate(req.body._id, gameVideo, function(err) {
        if (err) {
            var response = {};
                response.status ='false';
                response.msg = 'Something went wrong.';
                return res.json(response);
        } else {
            var response = {};
            response.status ='true';
            response.msg = 'Game Video updated successfully.';
            return res.json(response);
        }

    });
};
