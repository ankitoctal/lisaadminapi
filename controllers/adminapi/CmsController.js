const config = include('configs/config.js');
const Cms = include("models/Cms.js");
const GameStage = include("models/GameStage.js");
var mongoose = require('mongoose');

exports.add = function(req, res) { 
	var cms = new Cms();		
	cms.page_name = req.body.page_name;
	cms.page_title = req.body.page_title;
	cms.page_description = req.body.page_description;
	cms.meta_title = req.body.meta_title;
	cms.meta_description = req.body.meta_description;
	cms.meta_keywords = req.body.meta_keywords;
	cms.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Static Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.getDetails = function(req, res) {	
   	var query = Cms.find();        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.view = function(req, res) {	

   	var query = Cms.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.edit = function(req, res) { 
    var cms = {};
    cms.page_name = req.body.page_name;
	cms.page_title = req.body.page_title; 
	cms.page_description = req.body.page_description;
	cms.meta_title = req.body.meta_title;
	cms.meta_description = req.body.meta_description;
	cms.meta_keywords = req.body.meta_keywords;

    Cms.findByIdAndUpdate(req.body._id, cms, function(err) {
        if (err) {
            var response = {};
				response.status ='false';
				response.msg = 'Something went wrong.';
				return res.json(response);
        } else {
            var response = {};
			response.status ='true';
			response.msg = 'Static Page updated successfully.';
			return res.json(response);
        }

    });
};

exports.delete = function(req, res) {
    Cms.remove({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Static Page deleted successfully.';
	return res.json(response);
};

exports.updateStatus = function(req, res) {
	var cms = {};					 
	cms.status = req.params.status;			
	Cms.findByIdAndUpdate( req.params.id, cms , function (err) {
		var response = {};
		response.status ='true';
		response.msg = 'Status successfully changed.';
		return res.json(response);
	});
    
};


exports.addStage = function(req, res) { 
	var gamesatge = new GameStage();		
	gamesatge.stage = req.body.stage;
	gamesatge.gameId = req.body.gameId;
	gamesatge.video = req.body.video;
	gamesatge.title = req.body.title;
	gamesatge.saving_amount = req.body.saving_amount;
	gamesatge.researchCenter = req.body.researchCenter;
	gamesatge.strategyDetail = req.body.strategyDetail;

	gamesatge.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Stage Detail added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.getStageDetails = function(req, res) {	
   	var query = GameStage.find();        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.viewStage = function(req, res) {	

   	var query = GameStage.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.editStage = function(req, res) { 
    var gamesatge = {};

	gamesatge.stage = req.body.stage;
	gamesatge.gameId = req.body.gameId;
	gamesatge.video = req.body.video;
	gamesatge.title = req.body.title;
	gamesatge.saving_amount = req.body.saving_amount;
	gamesatge.researchCenter = req.body.researchCenter;
	gamesatge.strategyDetail = req.body.strategyDetail;


    GameStage.findByIdAndUpdate(req.body._id, gamesatge, function(err) {
        if (err) {
            var response = {};
				response.status ='false';
				response.msg = 'Something went wrong.';
				return res.json(response);
        } else {
            var response = {};
			response.status ='true';
			response.msg = 'Game Stage Content updated successfully.';
			return res.json(response);
        }

    });
};