const config = include('configs/config.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');

const Game = include("models/Game.js");


addGame = function(req, res) { 
    var game = new Game();
    game.budget = req.body.budget;
    game.floor = req.body.floor;
    game.title = req.body.title;
    game.saving_amount = req.body.saving_amount;
    game.description = req.body.description;

    game.save(function (err) {                         
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                  
        }else {
            var response = {};
            response.status ='true';
            response.msg = 'Game Detail added successfully.';
            return res.json(response);
        }   
                            
    });      
};

gameDetails = function(req, res) {  
    var query = Game.find();        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

viewGame = function(req, res) {    

    var query = Game.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

editGame = function(req, res) { 
    var game = {};

    game.budget = req.body.budget;
    game.floor = req.body.floor;
    game.title = req.body.title;
    game.slug = "game-manager-setting";
    game.description = req.body.description;
    game.saving_amount = req.body.saving_amount;


    Game.findByIdAndUpdate(req.body._id, game, function(err) {
        if (err) {
            var response = {};
                response.status ='false';
                response.msg = 'Something went wrong.';
                return res.json(response);
        } else {
            var response = {};
            response.status ='true';
            response.msg = 'Game Content updated successfully.';
            return res.json(response);
        }

    });
};

updateStatusGame = function(req, res){

    var object = {};                    
    object.status = req.params.status;         
    Game.findByIdAndUpdate(req.params.id, object , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });

};


module.exports = {
    addGame,
    viewGame,
    editGame,
    updateStatusGame,
    gameDetails
}

