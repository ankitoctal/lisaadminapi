const config = include('configs/config.js');
const Logo = include("models/Logo.js");
var mongoose = require('mongoose');
var multer  = require('multer')
var upload = multer({ dest: './public/uploads/logo/' })

exports.add = function(req, res) { 
	var logo = new Logo();		
	logo.logo = req.body.logo;
	logo.company = req.body.company;
	logo.link = req.body.link;
	logo.focus = req.body.focus;
    logo.employee = req.body.employee;
    logo.reach = req.body.reach;
	if(req.files !=""){
		let avatar = req.files.image;
		logo.logo = avatar.name;
		//upload.single(avatar.name);
		avatar.mv('./public/uploads/logo/'+""+avatar.name);
	}

	logo.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Logo added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.getDetails = function(req, res) {	
   	var query = Logo.find();        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view = function(req, res) {	

   	var query = Logo.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.edit = function(req, res) { 
    var logo = {};
	logo.company = req.body.company; 
	logo.link = req.body.link;
	logo.focus = req.body.focus;
    logo.employee = req.body.employee;
    logo.reach = req.body.reach;
	if(req.files !="" && req.files !=null ){  
		let avatar = req.files.image; 
		logo.logo = avatar.name;
		avatar.mv('./public/uploads/logo/'+""+avatar.name);
	}
    Logo.findByIdAndUpdate(req.body._id, logo, function(err) {
        if (err) {
            var response = {};
				response.status ='false';
				response.msg = 'Something went wrong.';
				return res.json(response);
        } else {
            var response = {};
			response.status ='true';
			response.msg = 'Logo updated successfully.';
			return res.json(response);
        }

    });
};

exports.delete = function(req, res) {
    Logo.remove({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Logo deleted successfully.';
	return res.json(response);
};
 
exports.updateStatus = function(req, res) {
	var logo = {};					 
	logo.status = req.params.status;			
	Logo.findByIdAndUpdate( req.params.id, logo , function (err) {
		var response = {};
		response.status ='true';
		response.msg = 'Status successfully changed.';
		return res.json(response);
	});
    
};