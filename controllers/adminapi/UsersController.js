const config = include('configs/config.js');
const User = include("models/User.js");
const Team = require('../../models/Team');
const bcrypt = require('bcrypt');
const CommonController=require('./CommonController');
let jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var randtoken = require('rand-token');

exports.check_email = function(req, res) {	

   	var query = User.findOne({'email':req.params.email});    
    query.sort({ created_at: -1 });
    query.exec(function (err, result) {
		if(result)
		{
			var response = {};				
			response.isEmailUnique =true;								
			return res.json(response);			
		}	
		else
		{
			var response = {};			
			return res.json(response);
		}  		
	}); 
	 	    
};

exports.test = function(req, res) {
	var response = {};
	response.status ='true';
	response.msg = 'Test Message';
	return res.json(response);	
}

exports.teamList = async function(req, res) { 
    var query = Team.find({}).select('_id team_name');        
        query.exec(function (err, result) {
            var response = {};              
            response.status ='success';                     
            response.data = result;
            return res.json(response);          
        });
}


exports.add = function(req, res) {

	if(req.body.email != "")
	{
		var invitationCode = '';
    
	    if(req.body.role_id == 2){
	        invitationCode = randtoken.generate(6);
	    }

		//var hashedPassword = passwordHash.generate(req.body.password);
		var user = new User();		
		user.role_id = req.body.role_id;
		user.first_name = req.body.first_name;
		user.last_name = req.body.last_name;
		user.email = req.body.email;
		user.position = req.body.position;
		// user.team_name = req.body.team_name;
		user.password = req.body.password;
		user.stage_leader = [];

		if (req.body.position == 1) {
            var teamData = { team_name: req.body.teamName };

            var teamInsert = new Team(teamData); 
                teamInsert.save();

            user.team_id  = teamInsert._id;
            user.invitaion_code  = invitationCode;
            
        }else{
            user.team_id  = req.body.team_id;
        }

		user.save(function (err) {							
			if(err)
			{
				var response = {};
				response.status ='error';
				response.msg = err.message;
				return res.json(response);			  		
			}
			else
			{
				/*var params =  {};
				params.slug='user_register';	
				params.to=req.body.email;				
				params.params = {};	
				params.params.name=req.body.name;		
				CommonController.sendMail(params);*/
	
				var response = {};
				response.status ='true';
				response.msg = 'Thank you for your registration';
				return res.json(response);	
			}			
					  			
		});		
	}
	else
	{
		var response = {};
		response.status ='error';
		response.msg = 'Email is required';
		return res.json(response);
	}     
};

exports.sendForgotPasswordMail = function(req, res) {
	
		if(req.body.email !="")
		{
			var user = new User();	
			User.findOne({email: req.body.email}, function(err,obj) {
				if(obj){
					var reset_pwd_link = config.site_url+"reset_password/"+Buffer.from(obj._id.toString()).toString('base64'); 
					var params =  {};
					params.slug='forget_password';	
					params.to=req.body.email;				
					params.params = {};	
					params.params.name=obj.name;	
					params.params.reset_pwd_link= reset_pwd_link;		
					CommonController.sendMail(params);
					var response = {};
					response.status ='success';
					response.msg = 'Please check your email to reset your password.';
					return res.json(response);		

				}else{
				 console.log(obj); 
				}
	 		});	
		}
		else
		{
			var response = {};
			response.status ='error';
			response.msg = 'Email is required';
			return res.json(response);
		}     
};


exports.resetUserPassword = function(req, res) {
	if(req.body.password !="" &&  req.body.password == req.body.confirm_password)
	{
		var hashedPassword = passwordHash.generate(req.body.password);
		User.update({_id: req.body.userid}, {
		    password: hashedPassword
		}, function(err, affected, resp) {
			if(err)
			{
				var response = {};
				response.status ='error';
				response.msg = err.message;
				return res.json(response);			  		
			}
			else
			{
				var response = {};
				response.status ='success';
				response.msg = 'Password reset successfully.';
				return res.json(response);	
			}	
		}); 
	}
	else
	{
		var response = {};
		response.status ='error';
		response.msg = 'Email is required';
		return res.json(response);
	}     
};


exports.addGuest = function(req, res) {
	if(req.body.email !="")
	{
		var hashedPassword = passwordHash.generate('123456');
		var user = new User();	
		User.findOne({email: req.body.email}, function(err,obj) { 
			if(obj){
						var response = {};
						response.status ='success';
						// response.msg = 'Thank you for your registration';
						response.data = obj;
						return res.json(response);
			}else{
				user.role_id = 3;
				user.name = 'Guest';
				user.email =req.body.email;
				user.password =hashedPassword;
				user.status = 1;
				user.save(function (err, result) {		
					if(err)
					{
						var response = {};
						response.status ='error';
						response.msg = err.message;
						return res.json(response);			  		
					}
					else
					{	
						var response = {};
						response.status ='success';
						// response.msg = 'Thank you for your registration';
						response.data = result;
						return res.json(response);
					}			
				});	
			}
		});
	}
	else
	{
		var response = {};
		response.status ='error';
		response.msg = 'Email is required';
		return res.json(response);
	}     
};


exports.login = async function(req, res) {
    let user = User.findOne({'email':req.body.email}, (err, user) =>  {
        if(err) throw err;
        if(!user) {
            res.json({
                'success': false,
                'message': 'Authentication Failed, User not found.',
            });
        } else if(user) {
            var validPassword = user.validPassword(req.body.password);
            if(!validPassword) {
                res.json({
                    'success': false,
                    'message': 'Authentication Faild, Wrong password.'
                });
            } else {
                var token = jwt.sign(
                    {user:user}, 
                    config.secret,
                    {'expiresIn': '24h'}
                );
                
                if(user.role_id != 1){ 
                	res.json({
	                    'success': false,
	                    'message': 'Authentication Faild, Wrong user.'
	                });
                }else{
                	var userData = {
                    name : user.first_name+' '+user.last_name,
	                    email: user.email
	                };
	                res.json({
	                    'success': true,
	                    'message': 'User login Successfully',
	                    'token': token,
	                    'user': userData,
	                });
                }
                
            }
        }
    });
}

exports.getUser = function(req, res) {	

   	var query = User.findOne({'_id':req.params.user_id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.getAllUser = function(req, res) {	
   	var query = User.find({'role_id':req.params.role_id});
   	query.populate('team_id');
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.saveUser = function(req, res) {
	
	var user = {};			
	user.first_name =req.body.first_name;	
	user.last_name =req.body.last_name;	
	User.findByIdAndUpdate( req.body._id, user , function (err) {
		var response = {};
		response.status ='true';
		response.msg = 'Profile Updated Successfully';
		return res.json(response);
	});		
};

exports.changePassword = function(req, res) {		
	
	var query = User.findOne({'_id':req.body.id});        	
	query.exec(function (err, result) {
		if(result != null)
		{
			var user = {};							
			user.password =req.body.password;	
			bcrypt.hash(user.password, 10, function(err, hash) {
		        if (err){
		        	var response = {};
					response.status ='error';			
					response.msg = err;
					return res.json(response);
		        }
		        // override the cleartext password with the hashed one
		        user.password = hash;
		        User.findByIdAndUpdate( req.body.id, user , function (err) {
					var response = {};
					response.status ='success';
					response.msg = 'Password Changed Successfully';
					return res.json(response);
				});
		    });
			
		}
		else
		{
			var response = {};
			response.status ='error';			
			response.msg = 'Old Password is not correct.';
			return res.json(response);	
		}
	});		
};

exports.edit = function(req, res) {
	//console.log(req.body);return false;
    var user = {};
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.email = req.body.email;
    user.team_name = req.body.team_name;
    if (req.body.password) {
        user.password =req.body.password;
    }

    User.findByIdAndUpdate(req.body._id, user, function(err) {
        if (err) {
            var response = {};
				response.status ='false';
				response.msg = 'Something went wrong.';
				return res.json(response);
        } else {
            var response = {};
			response.status ='true';
			response.msg = 'User updated successfully.';
			return res.json(response);
        }

    });
};

exports.delete = function(req, res) {
    User.remove({ _id: req.params.user_id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'User deleted successfully.';
	return res.json(response);
};