const config = include('configs/config.js');

const Question = include("models/Question.js");
const Answer = include("models/Answer.js");

exports.add = function(req, res) { 
	var objectQuestion = new Question();		
	objectQuestion.title = req.body.title;
	objectQuestion.question = req.body.question;
	// objectQuestion.building = req.body.building;
	objectQuestion.stage = req.body.stage;
	
	objectQuestion.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Question Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.edit = function(req, res) { 
    Question.findOne({_id: req.body._id}, function(err, content) {
    if(!err) {
        content.title = req.body.title;
        content.question = req.body.question;
		// content.building = req.body.building;
		content.stage = req.body.stage;
		
        content.save(function(err) {
            if(err)
                {
                    var response = {};
		            response.status ='false';
		            response.msg = 'Something went wrong.';
		            return res.json(response);                
                }else {
                    var response = {};
		            response.status ='true';
		            response.msg = 'Question updated successfully.';
		            return res.json(response);
                }   
        });
	}
	});
};

exports.getDetails = function(req, res) {	
   	var query = Question.find();   
   	// query.populate(['building']);
   	query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view =  function(req, res) {	  
   	var query = Question.findOne({'_id':req.params.id});        
    query.exec(async function (err, result) {
    	await Answer.find({'question_id':req.params.id}).exec(function (err, answers) {
				var response = {};				
				response.status ='success';						
				response.data = result;
				response.answers = answers;
				return res.json(response);
			});  		
	}); 	    
};


exports.delete = function(req, res) {
    Question.deleteOne({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Question deleted successfully.';
	return res.json(response);
};

exports.updateStatus = function(req, res) {
    var object = {};                    
    object.status = req.params.status;         
    Question.findByIdAndUpdate(req.params.id, object , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};