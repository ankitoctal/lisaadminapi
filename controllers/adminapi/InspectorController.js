const config = include('configs/config.js');
const Inspector = include("models/Inspector.js");

exports.add = function(req, res) { 
	var inspector = new Inspector();		
	inspector.full_name = req.body.full_name;
	inspector.designation = req.body.designation;
	inspector.bio = req.body.bio;
	if(req.files !=""){ 
		let avatar = req.files.image; 
		inspector.file = avatar.name;
		avatar.mv('./public/uploads/inspector/'+""+avatar.name);
	}

	inspector.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Inspector Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.edit = function(req, res) { 
    Inspector.findOne({_id: req.body._id}, function(err, content) {
    if(!err) {
        content.full_name = req.body.full_name;
		content.designation = req.body.designation;
		content.bio = req.body.bio;
		if(req.files !="" && req.files !=null){ 
			let avatar = req.files.image; 
			content.file = avatar.name;
			avatar.mv('./public/uploads/inspector/'+""+avatar.name);
		}
        content.save(function(err) {
            if(err)
                {
                    var response = {};
		            response.status ='false';
		            response.msg = 'Something went wrong.';
		            return res.json(response);                
                }else {
                    var response = {};
		            response.status ='true';
		            response.msg = 'Inspector updated successfully.';
		            return res.json(response);
                }   
        });
	}
	});
};
exports.getDetails = function(req, res) {	
   	var query = Inspector.find();   
   	query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view = function(req, res) {	
   	var query = Inspector.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};


exports.delete = function(req, res) {
    Inspector.deleteOne({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Inspector deleted successfully.';
	return res.json(response);
};

exports.updateStatus = function(req, res) {
    var inspector = {};                    
    inspector.status = req.params.status;         
    Inspector.findByIdAndUpdate(req.params.id, inspector , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};