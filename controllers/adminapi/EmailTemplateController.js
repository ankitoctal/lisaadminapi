const config = include('configs/config.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');

const EmailTemplate = include("models/Email.js");


getTemplateDetails = function(req, res) {   
    var query = EmailTemplate.find();        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

addTemplate = function(req, res) { 
    var template = new EmailTemplate();

    template.title = req.body.title;
    template.subject = req.body.subject;
    template.body = req.body.body;

    template.save(function (err) {                          
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                  
        }else {
            var response = {};
            response.status ='true';
            response.msg = 'Template added successfully.';
            return res.json(response);
        }   
                            
    });      
};


viewTemplate = function(req, res) { 
    var query = EmailTemplate.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

editTemplate = function(req, res) { 
    var template = {};
    template.title = req.body.title;
    template.subject = req.body.subject; 
    template.body = req.body.body;
    EmailTemplate.findByIdAndUpdate(req.body._id, template, function(err) {
        if (err) {
            var response = {};
                response.status ='false';
                response.msg = 'Something went wrong.';
                return res.json(response);
        } else {
            var response = {};
            response.status ='true';
            response.msg = 'Template updated successfully.';
            return res.json(response);
        }

    });
};


module.exports = {
    getTemplateDetails,
    viewTemplate,
    editTemplate,
    addTemplate
}

