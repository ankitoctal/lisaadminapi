const config = include('configs/config.js');
const Content = include("models/Content.js");
var mongoose = require('mongoose');

exports.add = function(req, res) { 
	var content = new Content();		
	content.page_name = req.body.page_name;
	content.start_date= req.body.start_date;
	content.end_date = req.body.end_date;
	content.main_page_title = req.body.main_page_title;
	content.main_page_subtitle = req.body.main_page_subtitle;
	content.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Content Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.getDetails = function(req, res) {	
   	var query = Content.find();   
   	query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view = function(req, res) {	
   	var query = Content.findOne({'slug':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.edit = function(req, res) { 
    Content.findOne({slug: req.body.slug}, function(err, content) {
    if(!err) {
        content.page_name = req.body.page_name;
        content.start_date= req.body.start_date;
        content.end_date = req.body.end_date;
        content.main_page_title = req.body.main_page_title;
        content.main_page_subtitle = req.body.main_page_subtitle;
        content.save(function(err) {
            if(err)
                {
                    var response = {};
		            response.status ='false';
		            response.msg = 'Something went wrong.';
		            return res.json(response);                
                }else {
                    var response = {};
		            response.status ='true';
		            response.msg = 'Content updated successfully.';
		            return res.json(response);
                }   
        });
	}
	});
};

exports.delete = function(req, res) {
    Content.deleteOne({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Content deleted successfully.';
	return res.json(response);
};
