const config = include('configs/config.js');

const Answer = include("models/Answer.js");


exports.add = function(req, res) { 
	var ans = new Answer();		
	ans.answer = req.body.answer;
	ans.question_id = req.body.question_id;
	ans.price = req.body.price;
	ans.choice = req.body.choice;
	
	ans.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Answer Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.edit = function(req, res) { 
    Answer.findOne({_id: req.body._id}, function(err, content) {
    if(!err) {
        content.answer = req.body.answer;
		content.question_id = req.body.question_id;
		content.price = req.body.price;
		content.choice = req.body.choice;
		
        content.save(function(err) {
            if(err)
                {
                    var response = {};
		            response.status ='false';
		            response.msg = 'Something went wrong.';
		            return res.json(response);                
                }else {
                    var response = {};
		            response.status ='true';
		            response.msg = 'Answer updated successfully.';
		            return res.json(response);
                }   
        });
	}
	});
};

exports.getDetails = function(req, res) {	
   	var query = Answer.find();   
   	query.populate(['building']);
   	query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view = function(req, res) {	  
   	var query = Answer.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};


exports.delete = function(req, res) {
    Answer.deleteOne({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Answer deleted successfully.';
	return res.json(response);
};

exports.updateStatus = function(req, res) {
    var object = {};                    
    object.status = req.params.status;         
    Answer.findByIdAndUpdate(req.params.id, object , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};