const config = include('configs/config.js');
const Building = include("models/Building.js");
var multer  = require('multer')
var upload = multer({ dest: 'public/uploads/building_files' })
exports.add = function(req, res) { 
	var building = new Building();		
	building.name = req.body.name;
	building.description = req.body.description;
	if(req.files !=""){ 
		let avatar = req.files.image; 
		building.file = avatar.name;
		avatar.mv('./public/uploads/building_files/'+""+avatar.name);
	}
	building.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status ='true';
			response.msg = 'Building Page added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.edit = function(req, res) { 
    Building.findOne({_id: req.body._id}, function(err, content) {
    if(!err) {
        content.name = req.body.name;
        content.description = req.body.description;
		if(req.files !="" && req.files !=null ){  
			let avatar = req.files.image; 
			content.file = avatar.name;
			avatar.mv('./public/uploads/building_files/'+""+avatar.name);
		}
        content.save(function(err) {
            if(err)
                {
                    var response = {};
		            response.status ='false';
		            response.msg = 'Something went wrong.';
		            return res.json(response);                
                }else {
                    var response = {};
		            response.status ='true';
		            response.msg = 'Building updated successfully.';
		            return res.json(response);
                }   
        });
	}
	});
};

exports.getDetails = function(req, res) {	
   	var query = Building.find();   
   	query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};
exports.view = function(req, res) {	  
   	var query = Building.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};


exports.delete = function(req, res) {
    Building.deleteOne({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Building deleted successfully.';
	return res.json(response);
};

exports.updateStatus = function(req, res) { console.log(req.params.id); console.log(req.params.status);
    var contractor = {};                    
    contractor.status = req.params.status;         
    Building.findByIdAndUpdate(req.params.id, contractor , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};