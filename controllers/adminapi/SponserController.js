const config  = include('configs/config.js');
const Sponser = include("models/Sponser.js");
var mongoose  = require('mongoose');
var multer    = require('multer')
var upload 	  = multer({ dest: './public/uploads/sponser/' })

exports.add = function(req, res) { 
	var sponser = new Sponser();
	
	sponser.stage = req.body.stage;
	sponser.name = req.body.name;

	if(req.files != ""){
		let avatar = req.files.image;
		sponser.logo = avatar.name;
		avatar.mv('./public/uploads/sponser/'+avatar.name);
	}

	sponser.save(function (err) {							
		if(err)
		{
			var response = {};
			response.status ='error';
			response.msg = err.message;
			return res.json(response);			  		
		}else {
            var response = {};
			response.status = 'true';
			response.msg = 'Sponser added successfully.';
			return res.json(response);
        }	
				  			
	});		 
};

exports.getDetails = function(req, res) {	
   	var query = Sponser.find();        
    query.exec(function (err, result) {
		var response = {};				
		response.status = 'success';						
		response.data = result;
		return res.json(response);	  		
	}); 	    
};

exports.view = function(req, res) {	

   	var query = Sponser.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
		var response = {};				
		response.status ='success';						
		response.data = result;
		return res.json(response);	  		
	});
};

exports.edit = function(req, res) { 
    var sponser = {};

    sponser.stage = req.body.stage;
	sponser.name = req.body.name;
	console.log(req.body);
	if(req.files != undefined && req.files != ""){
		console.log(req.files);
		let avatar = req.files.image;
		sponser.logo = avatar.name;
		avatar.mv('./public/uploads/sponser/'+avatar.name);
	}


    Sponser.findByIdAndUpdate(req.body._id, sponser, function(err) {
        if (err) {
            var response = {};
				response.status ='false';
				response.msg = 'Something went wrong.';
				return res.json(response);
        } else {
            var response = {};
			response.status ='true';
			response.msg = 'sponser updated successfully.';
			return res.json(response);
        }

    });
};

exports.delete = function(req, res) {
    Sponser.remove({ _id: req.params.id }).exec();
    var response = {};
	response.status ='true';
	response.msg = 'Sponser deleted successfully.';
	return res.json(response);
};
 
exports.updateStatus = function(req, res) {
	var logo = {};					 
	logo.status = req.params.status;			
	Sponser.findByIdAndUpdate( req.params.id, logo , function (err) {
		var response = {};
		response.status ='true';
		response.msg = 'Status successfully changed.';
		return res.json(response);
	});  
};