const config = include('configs/config.js');
const PlacementCategory = include("models/PlacementCategory.js");
exports.add = function(req, res) { 
    var object = new PlacementCategory();        
    object.name = req.body.name;
    object.save(function (err) {                            
        if(err)
        {
            var response = {};
            response.status ='error';
            response.msg = err.message;
            return res.json(response);                  
        }else {
            var response = {};
            response.status ='true';
            response.msg = 'Placement Category Page added successfully.';
            return res.json(response);
        }   
                            
    });      
};

exports.edit = function(req, res) { 
    PlacementCategory.findOne({_id: req.body._id}, function(err, content) {
        if(!err) {
            content.name = req.body.name;
            content.save(function(err) {
                if(err)
                {
                    var response = {};
                    response.status ='false';
                    response.msg = 'Something went wrong.';
                    return res.json(response);                
                }else {
                    var response = {};
                    response.status ='true';
                    response.msg = 'Placement Category updated successfully.';
                    return res.json(response);
                }   
            });
        }
    });
};

exports.getDetails = function(req, res) {   
    var query = PlacementCategory.find();   
    query.sort({ created_at: -1 });     
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};
exports.view = function(req, res) {   
    var query = PlacementCategory.findOne({'_id':req.params.id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};


exports.delete = function(req, res) {
    PlacementCategory.deleteOne({ _id: req.params.id }).exec();
    var response = {};
    response.status ='true';
    response.msg = 'Placement Category deleted successfully.';
    return res.json(response);
};

exports.updateStatus = function(req, res) {
    var object = {};                    
    object.status = req.params.status;         
    PlacementCategory.findByIdAndUpdate(req.params.id, object , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Status successfully changed.';
        return res.json(response);
    });
    
};