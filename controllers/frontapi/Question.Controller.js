const { Validator } = require('node-input-validator');
const config = include('configs/config.js');

const Question = include("models/Question.js");
const Answer = include("models/Answer.js");
const QuestionResult = include("models/QuestionResult.js");
const mongoose = require('mongoose');


getIssue = async function(req, res, next) {
    const request = req.body;
    let issueList = [];

    const validation = new Validator(request,{
        stage:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        return res.json({
            'status': 2,
            'error':  validation.errors
        });
    }


    await Question.aggregate([ 
                    { $match: {'stage': request.stage} },
                    { $lookup: {from: 'answers',localField: "_id",foreignField: "question_id",as: 'answers'} }
                    ]).exec(function(err, result) {
                        
                        if(result.length == 0) {
                            return res.json({
                                    'status': 0,
                                    'message': 'No record Found.'
                                });
                        }else{
                            return res.json({
                                    'status': 1,
                                    'message': 'Record Found successfully.',
                                    'details': result
                                });
                        }
                    });
}

saveIssueResult = async function(req, res, next) {
    const request = req.body;

    const validation = new Validator(request,{
        stage:"required",
        team_id:"required",
        game_id:"required",
        result:'required'
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        return res.json({
                'status': 2,
                'message': "Required Paramter Missing."
            });
    }

    await QuestionResult.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, response) => {
        if(response) {
            return res.json({
                    'status': 1,
                    'message': 'Record already exist.'
                });
        } else {
            let selectedAnswers = { team_id:request.team_id,game_id:request.game_id,stage:request.stage,answers:request.result,}

            var saveData = new QuestionResult(selectedAnswers); 
            saveData.save();
             
            return res.json({
                    'status': 1,
                    'message': 'Result saved successfull.'
                });
        }
    });
    
}

getIssueResult = async function(req, res, next) {
    const request = req.body;
    let list = [];
    let detail = [];
    var good = 0;
    var better = 0;
    var best = 0;
    var goodPrice = 0;
    var betterPrice = 0;
    var bestPrice = 0;
    var totalPrice = 0;

    const validation = new Validator(request,{
        stage:"required",
        team_id:"required",
        stage:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        return res.json({
                'status': 2,
                'message': "Required Paramter Missing."
            });
    }


    await QuestionResult.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, response) => {
        if(!response) {
            return res.json({
                        'status': 0,
                        'message': 'No record Found.'
                    });
        }else{
            detail = response.answers;
        }
    });

    let obj = {};

    for (var i = 0; i <  detail.length; i++) {

        await Answer.findOne({'_id':detail[i].ans_id},(err, record) => {
            if (record) {
                obj[record.choice] = new Array();
                if (record.choice == 'Good') { 
                    good = good + 1; 
                    goodPrice = goodPrice + parseInt(record.price);
                    obj[record.choice].push({'total':good,'price':goodPrice});
                }

                if (record.choice == 'Better') {
                    better = better + 1; 
                    betterPrice = betterPrice + parseInt(record.price);
                    obj[record.choice].push({'total':better,'price':betterPrice});
                }

                if (record.choice == 'Best') {
                    best = best + 1; 
                    bestPrice = bestPrice + parseInt(record.price);
                    obj[record.choice].push({'total':best,'price':bestPrice});
                }
            
                totalPrice = parseInt(goodPrice) + parseInt(betterPrice) + parseInt(bestPrice);
            }
        });
    }

    if (obj) {
        return res.json({
                    'status': 1,
                    'message': 'Record Found successfull.',
                    'detail':obj,
                    'totalPrice':totalPrice,
                    'totalAnswer':i
                });
    } else {
         return res.json({
                    'status': 0,
                    'message': 'No record Found.'
                });   
    }
}

module.exports = {
    getIssue,
    saveIssueResult,
    getIssueResult
}
