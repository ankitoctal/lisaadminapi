const { Validator } = require('node-input-validator');
const config = include('configs/config.js');
const GameStage = require('../../models/GameStage');
const GameStageResearch = require('../../models/GameStageResearch');
const GameStageWorkforce = require('../../models/GameStageWorkforce');
const Contractor = require('../../models/Contractor');
const User = require('../../models/User');
const Game = require('../../models/Game');
const Team = include("models/Team.js");
const TeamGameStatus = include("models/TeamGameStatus.js");
const StageScore = include("models/StageScore.js");

const Answer = include("models/Answer.js");
const QuestionResult = include("models/QuestionResult.js");
const PlacementAnswer = include("models/PlacementAnswer.js");
const mongoose = require('mongoose');

getStageIntro = async function(req, res, next) {
    const request = req.body;

    const validation = new Validator(request,{
        stage:"required",
        gameId:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }


    var count = await GameStage.findOne({'stage': request.stage,'gameId': request.gameId},(err, stageDetail) => {
        
                        if(!stageDetail) {
                            res.json({
                                'status': 0,
                                'message': 'No record Found.'
                            });
                        } else {
                                        
                            res.json({
                                'status': 1,
                                'message': 'Record Found successfull',
                                'detail': stageDetail
                            });
                        }
                    });
}

getStageResearchContent = async function(req, res, next) {

    const request = req.body;

    const validation = new Validator(request,{
        stage:"required",
        gameId:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }


    var count = await GameStageResearch.findOne({'stage': request.stage,'gameId': request.gameId},(err, stageDetail) => {
        
        if(!stageDetail) {
            res.json({
                'status': 0,
                'message': 'No record Found.'
            });
        } else {
                        
            res.json({
                'status': 1,
                'message': 'Record Found successfull',
                'detail': stageDetail
            });
        }
    });

}

getTeamList = async function(req, res, next) {
        const request = req.body;
        let team = {};

        const validation = new Validator(request,{
            team_id:"required"
        });

           
        const matched = await validation.check();
     
        if (!matched) {
            res.json({
                'status': 2,
                'error':  "No Team Found Related this Member."
            })
           
            return;
        }

        await Team.findOne({'_id': request.team_id},{team_name:1},(err, teamDetail) => {
                        
            if(!teamDetail) {

                res.json({
                    'status': 0,
                    'message': 'Team Detail Not Found.'
                });

                return;

            } else {
                team = teamDetail;
            }
        });


        await User.find({'team_id': request.team_id,'role_id': '2'},(err, teamList) => {
                        
            if(!teamList) {
                res.json({
                    'status': 0,
                    'message': 'No record Found.'
                });
                return;
            } else {
                            
                res.json({
                    'status': 1,
                    'message': 'Record Found successfull',
                    'userList': teamList,
                    'team': team
                });
                return;
            }
        });
}

getStageList = async function(req,res,next) {
    const request = req.body;
    const stageLeaders = [];

    const validation = new Validator(request,{
        team_id:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }

    await User.find({'team_id': request.team_id},{stage_leader:1},(err, stageLeader) => {

            if(!stageLeader) {
                res.json({
                    'status': 0,
                    'message': 'No record Found.'
                });
            } else {

                stageLeader.forEach (leader => {
                    if (leader.stage_leader) {
                        leader.stage_leader.forEach (value =>{
                            stageLeaders.push(value)
                        })
                    }
                })
                      
                res.json({
                    'status': 1,
                    'message': 'Record Found successfull',
                    'stageLeader': stageLeaders
                });
            }
        });

}

assignStageLeader = async function(req,res,next) {

    const request = req.body;
    var stageNumber = [];

    var conditions = { _id: request.user_id };
    if (request.stage1 != '') {
        stageNumber.push(1);
    }
    if (request.stage2 != '') {
        stageNumber.push(2);
    }
    if (request.stage3 != '') {
        stageNumber.push(3);
    }
    if (request.stage4 != '') {
        stageNumber.push(4);
    }

    var newvalues = { $set: {stage_leader : stageNumber} };
   
    await User.update(conditions,newvalues,(err, result) => {
    
            if(!result) {
                res.json({
                    'status': 0,
                    'message': 'Leader Not assign for this stage.'
                });
            } else {
                            
                res.json({
                    'status': 1,
                    'message': 'Leader Assign to stage',
                    'stageLeader': result
                });
            }
        });

}


teamList = async function(req, res) { 
        var query = Team.find({}).select('_id team_name');
        query.exec(function (err, result) {
            var response = {};              
            response.status ='success';                     
            response.data = result;
            return res.json(response);          
        });

}

teamMemberCount = async function(req,res,next){

    const request = req.body;
   
    await User.count({'team_id': request.team_id},(err, totalMember) => {

            if(!totalMember) {
                res.json({
                    'status': 0,
                    'message': 'No record Found.'
                });
            } else {
   
                res.json({
                    'status': 1,
                    'message': 'Record Found successfull',
                    'totalMember': totalMember
                });
            }
        });

}

saveWorkforce = async function(req,res,next){

    const request = req.body;

    const validation = new Validator(request,{
        team_id:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  "No Team Found Related this Member."
        })
       
        return;
    }

    var workforceData = { team_id: request.team_id};
    if (request.concrete != '') {
        workforceData.concrete = request.concrete;
    }
    if (request.electrical != '') {
        workforceData.electrical = request.electrical;
    }
    if (request.mechanical != '') {
        workforceData.mechanical = request.mechanical;
    }
    if (request.plumbing != '') {
        workforceData.plumbing = request.plumbing;
    }
    if (request.drywall != '') {
        workforceData.drywall = request.drywall;
    }
    if (request.curtainwall != '') {
        workforceData.curtainwall = request.curtainwall;
    }
    if (request.finish != '') {
        workforceData.finish = request.finish;
    }
    

    await GameStageWorkforce.findOne({'team_id':request.team_id},(err, user) => {
        if(user) {

            GameStageWorkforce.findByIdAndUpdate(user._id, workforceData, function(err) {
                if (err) {
                    res.json({
                        'status': 0,
                        'message': 'Something went wrong.Please try Again'
                    })
                }else{
                    res.json({
                        'status': 1,
                        'message': 'Workforce Updated successfull.'
                    })   
                }
            });
        } else {

            var saveData = new GameStageWorkforce(workforceData); 
            saveData.save();
             
            res.json({
                'status': 1,
                'message': 'Workforce saved successfull.'
            })
        }
    });

}

getWorkforce  = async function(req,res,next){

    const request = req.body;

    await GameStageWorkforce.findOne({'team_id':request.team_id}).populate('concrete electrical mechanical plumbing drywall curtainwall finish').exec(function(err, workforce) {
        
        if(!workforce) {
            res.json({
                'status': 0,
                'message': 'No Record found.'
            });
        } else {

            totalcost = Number(workforce.concrete.cost)+Number(workforce.electrical.cost)+Number(workforce.mechanical.cost)+Number(workforce.plumbing.cost)+Number(workforce.drywall.cost)+Number(workforce.curtainwall.cost)+Number(workforce.finish.cost);

            avgSpeed = (Number(workforce.concrete.speed)+Number(workforce.electrical.speed)+Number(workforce.mechanical.speed)+Number(workforce.plumbing.speed)+Number(workforce.drywall.speed)+Number(workforce.curtainwall.speed)+Number(workforce.finish.speed))/7;

            avgQuality = (Number(workforce.concrete.quality)+Number(workforce.electrical.quality)+Number(workforce.mechanical.quality)+Number(workforce.plumbing.quality)+Number(workforce.drywall.quality)+Number(workforce.curtainwall.quality)+Number(workforce.finish.quality))/7;

            avgSafty = (Number(workforce.concrete.safty)+Number(workforce.electrical.safty)+Number(workforce.mechanical.safty)+Number(workforce.plumbing.safty)+Number(workforce.drywall.safty)+Number(workforce.curtainwall.safty)+Number(workforce.finish.safty))/7;
            
           
            res.json({
                'status': 1,
                'message': 'record found successfull.',
                'detail' : workforce,
                'totalCost' : totalcost,
                'avgSpeed' : avgSpeed,
                'avgQuality' : avgQuality,
                'avgSafty' : avgSafty
            })
        }
    });

}

getContractors = async function(req,res,next){

    const request = req.body;

    let list = [];

    var query = Contractor.find({}).sort( { category: 1 });

        await query.exec(function (err, contractors) {
        
            if(!contractors) {
                res.json({
                    'status': 0,
                    'message': 'No Record found.'
                });
            } else {
                let obj = {};
                contractors.forEach (data => {
                    if (list.indexOf(data.category) != -1) {
                        obj[data.category].push(data);
                    }else{
                        list.push(data.category);
                        obj[data.category] = new Array();
                        obj[data.category].push(data); 
                    }
                })

                res.json({
                    'status': 1,
                    'message': 'record found successfull.',
                    'detail' : obj
                })
            }
        });

}

getGameDetail = async function(req,res,next){

    const request = req.body;

    await Game.findOne({'status':'1'}).exec(function(err, game) {
        
        if(!game) {
            res.json({
                'status': 0,
                'message': 'No Record found.'
            });
        } else {

            res.json({
                'status': 1,
                'message': 'record found successfull.',
                'detail' : game
            })
        }
    });
}

getContractor = async function(req,res,next){

    const request = req.body;

    var query = Contractor.findOne({_id:request.id}, (err, contractor) => {
        
            if(!contractor) {
                res.json({
                    'status': 0,
                    'message': 'No Record found.'
                });
            } else {
                
                res.json({
                    'status': 1,
                    'message': 'record found successfull.',
                    'detail' : contractor
                })
            }
        });

}

savedScore = async function(req,res,next){
    const request = req.body;

    const validation = new Validator(request,{
        team_id:"required"
    });

    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  "Your Stage not Complete. Please try Again."
        })
       
        return;
    }

    await StageScore.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, score) => {
        if(score) {

            StageScore.findByIdAndUpdate(score._id, request, function(err) {
                if (err) {
                    res.json({
                        'status': 0,
                        'message': 'Something went wrong.Please try Again'
                    })
                }else{
                    res.json({
                        'status': 1,
                        'message': 'Score Updated successfull.'
                    })   
                }
            });
        } else {

            var saveData = new StageScore(request); 
            saveData.save();
             
            res.json({
                'status': 1,
                'message': 'Score saved successfull.'
            })
        }
    });
}

getTeamStageStatus = async function(req,res,next){

    const request = req.body;

    const validation = new Validator(request,{
        team_id:"required"
    });

    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  "Something went wrong. Please try Again."
        })
       
        return;
    }

    await StageScore.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, stageStatus) => {
        if(stageStatus) {
            if (stageStatus.status == 'Complete') {
                return res.json({
                    'status': 3,
                    'message': 'Your Team already complete this stage. Please try another stage.'
                }) 
            }
        } else {
            TeamGameStatus.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, teamStageStatus) => {
                if(teamStageStatus) {
                    // status 0 = stage running 1 =  complete
                    if (teamStageStatus.status != 1) {
                        return res.json({
                            'status': 1,
                            'detail': teamStageStatus,
                            'message': 'Your Team pause this stage in mid. Please continue from that part.'
                        }); 
                    }else{
                        return res.json({
                            'status': 0,
                            'message': 'Please start game.'
                        });   
                    }
                } else {
                    return res.json({
                            'status': 0,
                            'message': 'Please start game.'
                        });
                }
            });
        }
    });
}

saveTeamStageStatus = async function(req,res,next){
    const request = req.body;

    const validation = new Validator(request,{
        team_id:"required"
    });

    const matched = await validation.check();
 
    if (!matched) {
        res.json({
            'status': 2,
            'error':  "Your Stage not Complete. Please try Again."
        })
       
        return;
    }

    await TeamGameStatus.findOne({'team_id':request.team_id,'game_id':request.game_id},(err, score) => {
        if(score) {

            TeamGameStatus.findByIdAndUpdate(score._id, {'stage':request.stage,'remaining_time':request.remaining_time,'current_route':request.current_route}, function(err) {
                if (err) {
                    res.json({
                        'status': 0,
                        'message': 'Something went wrong.Please try Again'
                    })
                }else{
                    res.json({
                        'status': 1,
                        'message': 'Record update Success.'
                    })   
                }
            });
        } else {

            var saveData = new TeamGameStatus(request); 
            saveData.save();
             
            res.json({
                'status': 1,
                'message': 'Record saved successfull.'
            })
        }
    });   
}

getCompletedStage = async function(req,res,next){
    const request = req.body;

    await StageScore.find({'team_id':request.team_id,'game_id':request.game_id},(err, stageStatus) => {
        if (stageStatus.length == 0) {
            return res.json({
                    'status': 0,
                    'message': 'No Record found.'
                });
        } else {
            var lastCompletedStage;

            for (var i = 0; i <  stageStatus.length; i++) {
                lastCompletedStage = stageStatus[i].stage; 
            }

            return res.json({
                    'status': 1,
                    'message': 'record found.',
                    'detail':stageStatus,
                    'lastCompletedStage':lastCompletedStage
                });   
        }
    });
}

getStageDetails = async function(req,res,next){
    const request = req.body;
    let timeSavingDetails = {};
    let obj = {};
    var good = 0;
    var better = 0;
    var best = 0;
    var goodPrice = 0;
    var betterPrice = 0;
    var bestPrice = 0;

    await StageScore.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, stageStatus) => {
        if (stageStatus.length == 0) {
            return res.json({
                    'status': 0,
                    'message': 'No Record found.'
                });
        } else {
            timeSavingDetails = {'savedMin':stageStatus.savedMin,'price':stageStatus.min_price};
        }
    });

    if (request.stage == '1') {
    	return res.json({
	                'status': 1,
	                'message': 'record found.',
	                'detail':timeSavingDetails,
	                'answer':obj
	            });
    }

    if (request.stage == '3' || request.stage == '4') {
        await QuestionResult.findOne({'team_id':request.team_id,'game_id':request.game_id,'stage':request.stage},(err, response) => {
            if(response) {
                detail = response.answers;
            }
        });

        for (var i = 0; i <  detail.length; i++) {

            await Answer.findOne({'_id':detail[i].ans_id},(err, record) => {
                if (record) {
                    obj[record.choice] = new Array();
                    if (record.choice == 'Good') {
                        good = good + 1; 
                        goodPrice = record.price;
                        obj[record.choice].push({'total':good,'price':goodPrice});
                    }

                    if (record.choice == 'Better') {
                        better = better + 1; 
                        betterPrice = record.price;
                        obj[record.choice].push({'total':better,'price':betterPrice});
                    }

                    if (record.choice == 'Best') {
                        best = best + 1; 
                        bestPrice = record.price;
                        obj[record.choice].push({'total':best,'price':bestPrice});
                    }
                }
            });
        }

        return res.json({
	                'status': 1,
	                'message': 'record found.',
	                'detail':timeSavingDetails,
	                'answer':obj
	            });
   
    }

    if (request.stage == '2') {
        await PlacementAnswer.findOne({'team_id':request.team_id,'game_id':request.game_id}).populate('material crane trailer ingress').exec(function(err, answers) {
                if(answers) {
                    obj['Good'] = new Array();
                    obj['Better'] = new Array();
                    obj['Best'] = new Array();

                    if (answers.material.answer == 'Good') {
                        good = good + 1; 
                        goodPrice = '100000';
                    }

                    if (answers.material.answer == 'Better') {
                        better = better + 1; 
                        betterPrice = '200000';
                    }
                    if (answers.material.answer == 'Best') {
                        best = best + 1; 
                        bestPrice = '300000';
                    }

                    if (answers.crane.answer == 'Good') {
                        good = good + 1; 
                        goodPrice = '100000';
                    }

                    if (answers.crane.answer == 'Better') {
                        better = better + 1; 
                        betterPrice = '200000';
                    }
                    if (answers.crane.answer == 'Best') {
                        best = best + 1; 
                        bestPrice = '300000';
                    }


                    if (answers.trailer.answer == 'Good') {
                        good = good + 1; 
                        goodPrice = '100000';
                    }

                    if (answers.trailer.answer == 'Better') {
                        better = better + 1; 
                        betterPrice = '200000';
                    }
                    if (answers.trailer.answer == 'Best') {
                        best = best + 1; 
                        bestPrice = '300000';
                    }

                    if (answers.ingress.answer == 'Good') {
                        good = good + 1; 
                        goodPrice = '100000';
                    }

                    if (answers.ingress.answer == 'Better') {
                        better = better + 1; 
                        betterPrice = '200000';
                    }
                    if (answers.ingress.answer == 'Best') {
                        best = best + 1; 
                        bestPrice = '300000';
                    }

                    obj['Good'].push({'total':good,'price':goodPrice});
                    obj['Better'].push({'total':better,'price':betterPrice});
                    obj['Best'].push({'total':best,'price':bestPrice});
                }

                return res.json({
	                'status': 1,
	                'message': 'record found.',
	                'detail':timeSavingDetails,
	                'answer':obj
	            });
        });
    }     
}

module.exports = {
    getStageIntro,
    getTeamList,
    getStageList,
    assignStageLeader,
    teamList,
    teamMemberCount,
    getStageResearchContent,
    saveWorkforce,
    getWorkforce,
    getContractors,
    getGameDetail,
    getContractor,
    savedScore,
    getTeamStageStatus,
    saveTeamStageStatus,
    getCompletedStage,
    getStageDetails
}
