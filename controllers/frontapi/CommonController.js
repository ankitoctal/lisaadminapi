const { Validator } = require('node-input-validator');
const config = include('configs/config.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');
const Logo = include("models/Logo.js");
const GameVideo = include("models/GameVideo.js");
const Chat = include("models/Chat.js");
const Cms = include("models/Cms.js");
const Content = include("models/Content.js");
const StageScore = include("models/StageScore.js");
const Game = include("models/Game.js");

const User = include("models/User.js");
const Placement = include("models/Placement.js");
const PlacementCategory = include("models/PlacementCategory.js");
const PlacementAnswer = include("models/PlacementAnswer.js");
const Sponser = include("models/Sponser.js");


exports.getLogoDetails = function(req, res) {   
    var query = Logo.find();        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};
exports.gameVideos = function(req, res) {  
    var videoType = req.params.video_type; 
    var query = GameVideo.find({'category':videoType});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

exports.getUserChats = function(req, res) {
    var user_team_id = req.params.user_team_id;   
    var query = Chat.find({'team_id':user_team_id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};
exports.showCms = function(req, res) { 
    var slug = req.params.user_team_id;
    var query = Cms.findOne({'slug':req.params.slug});
    var team_id = req.params.user_team_id;   
    var query = Chat.find({'team_id':team_id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};
exports.showCms = function(req, res) { 
    var slug = req.params.user_team_id;
    var query = Cms.findOne({'slug':req.params.slug});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
}; 

exports.getContentName = function(req, res) { 
    var query = Content.findOne({'slug':req.params.slug});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

exports.getLeaderboard = function(req,res,next){
    const request = req.body;
    const leaderBoard = [];
    const obj = [];

    var query = StageScore.find({'game_id':request.game_id}).populate('team_id game_id');        
    query.exec(function (err, result) {

        if (!result) {
            return res.json({
                'status': 0,
                'message': 'No Record Found.'
            });
        }else {

            result.forEach (data => {
                if (obj.indexOf(data.team_id._id) != -1) {

                    leaderBoard.forEach (scoreDetails => {
                        if (scoreDetails.team_id === data.team_id._id) {
                            scoreDetails.total_spent =  parseInt(scoreDetails.total_spent) + parseInt(data.total_spent);
                            scoreDetails.score =  parseInt(scoreDetails.score) + parseInt(data.score);
                        }
                    });

                }else{

                    leaderBoard.push({ 'team_id': data.team_id._id , 'team_name': data.team_id.team_name, 'total_spent': data.total_spent, 'score': data.score, 'budget':data.game_id.budget });
                    obj.push(data.team_id._id); 
                }
            });

            return res.json({
                'status': 1,
                'details':  leaderBoard,
                'message': 'record Found.'
            });
        }

      
                       
    }); 

}

exports.updateTimer = function(req, res) {
    var user_id = req.params.user_id;
    var user = {};          
    user.game_time = req.params.time;   
    User.findByIdAndUpdate( user_id, user , function (err) {
        var response = {};
        response.status ='true';
        response.msg = 'Timer Updated Successfully';
        return res.json(response);
    }); 
};


exports.checkGameTimer = function(req, res) {
    var user_id = req.params.user_id;        
    var query = User.findOne({'_id':user_id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });
};

exports.getPlacementDetails = function(req, res) {   
    var query = Placement.find();   
    query.populate(['category']);
    query.sort({ updated_at: -1 });     
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });           
};

exports.getPlacementCategoryList = function(req, res) {   
    PlacementCategory.aggregate([
        {
            $lookup:
            {
                from: 'placements',
                localField: "_id",
                foreignField: "category",
                as: 'placements'
            }
        },
    ]).exec(function(err, result) {
        if (err) throw err;
        var response = {};              
            response.status ='success';                     
            response.data = result;
            return res.json(response); 
    });          
};


exports.saveSelectedPlacement = async function(req, res) {
    const request = req.body;
    
    const validation = new Validator(request,{
        team_id:"required",
        game_id:"required",
        material:"required",
        crane:"required",
        trailer:"required",
        ingress:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
       
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }

    var count = await PlacementAnswer.findOne({'team_id':request.team_id,'game_id':request.game_id},(err, answer) => {
        if(answer) {
            return res.json({
                'status': 0,
                'message': 'Placement is Already done By this Team.'
            });
        } else {

            var userData = { team_id: request.team_id, game_id: request.game_id, material: request.material, crane: request.crane, trailer: request.trailer, ingress: request.ingress};


            var userInsert = new PlacementAnswer(userData); 
            userInsert.save();
             
            return res.json({
                'status': 1,
                'message': 'Placement Complete successfull'
            });
        }
    });
};

exports.getSelectedPlacement = async function(req, res) {
    const request = req.body;
    
    const validation = new Validator(request,{
        team_id:"required",
        game_id:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
       
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }

    await PlacementAnswer.findOne({'team_id':request.team_id,'game_id':request.game_id}).populate('material crane trailer ingress').exec(function(err, answer) {
        if(!answer) {
            return res.json({
                'status': 0,
                'message': 'No Result Found'
            });
        } else {

            return res.json({
                'status': 1,
                'message': 'Record Found Successfully.',
                'detail' : answer
            });
        }
    });
}

exports.getGameManagerSetting = function(req, res) {    
    var query = Game.findOne({'slug':req.params.slug});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};


exports.getSponsers = function(req,res,next){
    var query = Sponser.findOne({'stage':req.body.stage});        
        query.exec(function (err, result) {
            if (result) {
                return res.json({
                    'status': 1,
                    'data'  : result,
                    'message': 'Record Found Successfully.'
                }); 
            } else {
                return res.json({
                    'status': 0,
                    'message': 'No record Found.'
                }); 
            }       
        });  
}