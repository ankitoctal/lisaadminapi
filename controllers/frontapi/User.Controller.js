const { Validator } = require('node-input-validator');
const config = include('configs/config.js');
const User = require('../../models/User');
const Team = require('../../models/Team');
const CommonController = require('../adminapi/CommonController');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
let jwt = require('jsonwebtoken');
const passwordResetToken = require('../../models/PasswordReset');
var randtoken = require('rand-token');

saveUser = async function(req, res, next) {
    const request = req.body;
    var invitationCode = '';

    
    if(request.role_id == 2){
        invitationCode = randtoken.generate(6);
    }

    const validation = new Validator(request,{
        firstName:"required|maxLength:50",
        lastName:"required|maxLength:50",
        email:"required|email",
        password:"required|minLength:6|same:confirmPassword",
        confirmPassword:"required|minLength:6",
        position:"required"
    });

       
    const matched = await validation.check();
 
    if (!matched) {
       
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }

    var count = await User.findOne({'email':request.email},(err, user) => {
        if(user) {
            res.json({
                'status': 0,
                'message': 'This Email is already been Exists'
            });
        } else {

            var userData = { first_name: request.firstName, last_name: request.lastName, position: request.position, password: request.password, email: request.email, role_id: request.role_id, stage_leader:[] };


            if (request.position == 1) {
                var teamData = { team_name: request.teamName };

                var teamInsert = new Team(teamData); 
                teamInsert.save();

                    userData.team_id  = teamInsert._id;
                    userData.invitaion_code  = invitationCode;
            }else{
                userData.team_id  = request.team_id;
            }

            var userInsert = new User(userData); 
            userInsert.save();
             
            res.json({
                'status': 1,
                'message': 'Registration successfull'
            })
        }
    });  
}

login = async function(req, res, next) {
    const request = req.body;

    const validation = new Validator(request,{
        email:"required|email",
        password:"required"
    });

    const matched = await validation.check();
 
    if (!matched) {
       
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }
    let user = User.findOne({'email':req.body.email}, (err, user) =>  {
        if(err) throw err;
        if(!user) {
            return res.json({
                'status': 0,
                'message': 'Authentication Failed, User not found.',
            });
        } else if(user) {
            var validPassword = user.validPassword(request.password);
            if(!validPassword) {
                return res.json({
                    'status': 0,
                    'message': 'Authentication Faild, Wrong password.'
                });
            } else {
                var token = jwt.sign(
                    {user:user}, 
                    config.secret,
                    {'expiresIn': '24h'}
                );
                var userData = {
                    name : user.first_name+' '+user.last_name,
                    email: user.email,
                    role_id: user.role_id,
                    team_id: user.team_id,
                    position: user.position,
                    user_id: user._id,
                    stage_leader:user.stage_leader
                };
            
                return res.json({
                    'status': 1,
                    'message': 'User login Successfully',
                    'token': token,
                    'user': userData,
                });
            }
        }
    });
}


sendInviteLink = async function(req, res, next) {

        const request = req.body;
        console.log(request);
        const validation = new Validator(request,{
            email:"required|email"
        });
        
        const matched = await validation.check();
     
        if (!matched) {
           
            res.json({
                'status': 2,
                'error':  validation.errors
            })
           
            return;
        }

        await Team.findOne({'_id':request.team_id},{team_name:1}, (err, teamName) =>  {
        
                if(!teamName) {
                    res.json({
                        'status': 0,
                        'message': 'Team Not Found.',
                    });
                } else if(teamName) {

                    var link = config.invitaiton_url+'?invitation_id='+teamName._id+'&email='+request.email+'&user_type='+request.user_role_id; 
                    console.log(link);
                    var params =  {link};
                    params.slug = 'invites';
                    params.to = request.email;
                    params.params = {};
                    params.params.TeamName = teamName.team_name;
                    params.params.Link = link;       
                    CommonController.sendMail(params);

                    res.json({
                        'status': 1,
                        'message': 'Inviation link send successfull'
                    });
                }
        })       
}

forgetPassword = async function(req, res, next) {

    const request = req.body;

    const validation = new Validator(request,{
        email:"required|email"
    });
    
    const matched = await validation.check();
 
    if (!matched) {
       
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }

    let user = User.findOne({'email':req.body.email}, (err, user) =>  {
        if(err) throw err;
        if(!user) {
            res.json({
                'status': 0,
                'message': 'This Email not matched with any user.',
            });
        } else if(user) {

            var resettoken = new passwordResetToken({ _userId: user._id, resettoken: crypto.randomBytes(16).toString('hex') });

            resettoken.save(function (err) {
                if (err) { 
                    return res.json({ 'status': 0,'message': err.message }); 
                }
                passwordResetToken.find({ _userId: user._id, resettoken: { $ne: resettoken.resettoken } }).remove().exec();

                var reset_pwd_link = config.front_site_url+"/reset-password/"+resettoken.resettoken; 
                var params =  {};
                params.slug = 'forget_password';
                params.to = req.body.email;
                params.params = {};
                params.params.name = user.first_name+' '+user.last_name;   
                params.params.reset_pwd_link = reset_pwd_link;       
                CommonController.sendMail(params);
                
                res.json({
                    'status': 1,
                    'message': 'Reset Password link send successfull',
                });
            });
        }
    });
}

ValidPasswordToken = async  function(req, res, next){

    const request = req.body;

    if (!request.resettoken) {

        return  res.json({
                'status': 0,
                'message': 'Invalid URL Or Token has expired'
            });

    }
        
    const user = await passwordResetToken.findOne({
                            resettoken: request.resettoken
                        });
    
    if (!user) {

        return res.json({
                    'status': 0,
                    'message': 'Invalid URL'
                });
    }
            
    User.findOneAndUpdate({ _id: user._userId }).then(() => {
                    return res.json({ 'status': 1, 'message': 'Token verified successfully.' });
                    
                }).catch((err) => {
                    return res.json({ 'status': 2,'error': err.message });
                });
}


resetPassword = async function(req, res, next) {
    
    const request = req.body;

    const validation = new Validator(request,{
        password:"required|minLength:6|same:confirmPassword",
        confirmPassword:"required|minLength:6",
    });
    
    const matched = await validation.check();
 
    if (!matched) {
       
        res.json({
            'status': 2,
            'error':  validation.errors
        })
       
        return;
    }

    passwordResetToken.findOne({ resettoken: request.resettoken }, function (err, userToken, next) {
        if (!userToken) {
            return res.json({ 'status': 0, 'message': 'Token has expired.' });
        }
        
        User.findOne({ _id: userToken._userId }, function (err, userEmail, next) {

            if (!userEmail) {
              return res.json({ 'status': 0, 'message': 'User does not exist' });
            }

            userEmail.password = request.password;
            userEmail.save(function (err) {
                if (err) {
                  return res.json({ 'status': 0,'message': 'Password can not reset.please try again later.' });
                } else {
                  userToken.remove();
                  return res.json({ 'status': 1, 'message': 'Password reset successfully' });
                }
            });
        });
    }); 
}

getUser = function(req, res) {  

    var query = User.findOne({'_id':req.params.user_id});        
    query.exec(function (err, result) {
        var response = {};              
        response.status ='success';                     
        response.data = result;
        return res.json(response);          
    });         
};

updateUser = function(req, res) {
    
    var user = {};          
    user.first_name =req.body.first_name;   
    user.last_name =req.body.last_name;   
    user.email =req.body.email; 
    User.findByIdAndUpdate( req.body._id, user , function (err) {
        var response = {};
        response.status ='success';
        response.msg = 'Profile Updated Successfully';
        return res.json(response);
    });     
};
changePassword = function(req, res) {   
    var query = User.findOne({'_id':req.body.id});          
    query.exec(function (err, result) {
        bcrypt.compare(req.body.old_password, result.password, function(err, res1) {
          if(res1) {
           var user = {};     
           bcrypt.hash(req.body.password, 10, function(err, hash) {
              user.password = hash; 
              User.findByIdAndUpdate( req.body.id, user , function (err) {
                    var response = {};
                    response.status ='success';
                    response.msg = 'Password Changed Successfully';
                    return res.json(response);
                });
            });                     
               
            
          } else {
               var response = {};
                response.status ='error';           
                response.msg = 'Old Password is not correct.';
                return res.json(response);
          } 
        });
        // if(result != null && passwordHash.verify(req.body.old_password, result.password))
        // {
        //     var user = {};                          
        //     user.password = req.body.password;    
        //     User.findByIdAndUpdate( req.body.id, user , function (err) {
        //         var response = {};
        //         response.status ='success';
        //         response.msg = 'Password Changed Successfully';
        //         return res.json(response);
        //     });
        // }
        // else
        // {
        //     var response = {};
        //     response.status ='error';           
        //     response.msg = 'Old Password is not correct.';
        //     return res.json(response);  
        // }
    });     
};
module.exports = {
    saveUser,
    login,
    forgetPassword,
    ValidPasswordToken,
    resetPassword,
    sendInviteLink,
    getUser,
    updateUser,
    changePassword
}
