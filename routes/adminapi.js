var express = require('express');
var router = express.Router();

var UsersController  =  require('../controllers/adminapi/UsersController');
var CmsController  =  require('../controllers/adminapi/CmsController');
var LogoController  =  require('../controllers/adminapi/LogoController');
var CommonController  =  require('../controllers/adminapi/CommonController');
var ContentManagerController  =  require('../controllers/adminapi/ContentManagerController');
var ContractorManagerController  =  require('../controllers/adminapi/ContractorManagerController');
var BuildingController  =  require('../controllers/adminapi/BuildingController');
var InspectorController  =  require('../controllers/adminapi/InspectorController');
var QuestionController  =  require('../controllers/adminapi/QuestionController');
var AnswerController  =  require('../controllers/adminapi/AnswerController');
var EmailTemplateController  =  require('../controllers/adminapi/EmailTemplateController');

var gameStageController = require('../controllers/adminapi/GameStageController');
var gameController = require('../controllers/adminapi/GameController');
var placementCategoryController = require('../controllers/adminapi/PlacementCategoryController');
var placementController = require('../controllers/adminapi/PlacementController');
var sponserController = require('../controllers/adminapi/SponserController');

// users routes
/*router.get('/users/check_email/:email', UsersController.check_email);*/


router.all('/team-list',UsersController.teamList);
router.all('/users/add', UsersController.add);
router.all('/users/test', UsersController.test);
router.get('/users/getalluser/:role_id', UsersController.getAllUser);
router.all('/users/login', UsersController.login);
router.all('/users/edit/:user_id', UsersController.edit);
router.all('/users/getuser/:user_id', UsersController.getUser);
router.get('/users/delete/:user_id', UsersController.delete);
router.all('/users/saveuser', UsersController.saveUser);
router.all('/users/changepassword', UsersController.changePassword);

/*CMS-manager routing start here*/
router.all('/cms-manager/add', CmsController.add);
router.get('/cms-manager/get-details', CmsController.getDetails);
router.get('/cms-manager/delete/:id', CmsController.delete);
router.get('/cms-manager/view/:id', CmsController.view);
router.all('/cms-manager/edit', CmsController.edit);
router.get('/cms-manager/update-status/:id/:status', CmsController.updateStatus);
/*CMS-manager routing finish here*/

/*  Stage Intro  */

router.all('/stage-intro/add', CmsController.addStage);
router.get('/stage-intro/get-details', CmsController.getStageDetails);
router.get('/stage-intro/view/:id', CmsController.viewStage);
router.all('/stage-intro/edit', CmsController.editStage);
/*  */

/*  Stage Intro  */

router.all('/stage-research/add', gameStageController.addStageResearch);
router.get('/stage-research/get-details', gameStageController.getStageResearchDetails);
router.get('/stage-research/view/:id', gameStageController.viewStageResearch);
router.all('/stage-research/edit', gameStageController.editStageResearch);
/*finrish*/

/*Placement-category routing start here*/
router.get('/placement-category/get-details', placementCategoryController.getDetails);
router.all('/placement-category/add', placementCategoryController.add);
router.all('/placement-category/edit', placementCategoryController.edit);
router.get('/placement-category/view/:id', placementCategoryController.view);
router.get('/placement-category/update-status/:id/:status', placementCategoryController.updateStatus);
/* fineshj */

/*Placement-category routing start here*/
router.all('/placements-manager/add', placementController.add);
router.get('/placements-manager/get-details', placementController.getDetails);
router.get('/placements-manager/view/:id', placementController.view);
router.all('/placements-manager/edit', placementController.edit);
router.get('/placements-manager/delete/:id', placementController.delete);
router.get('/placements-manager/update-status/:id/:status', placementController.updateStatus);
/* fineshj */


/* Email Template Route Start  */

router.all('/email-template/add', EmailTemplateController.addTemplate);
router.get('/email-template/get-details', EmailTemplateController.getTemplateDetails);
router.get('/email-template/view/:id', EmailTemplateController.viewTemplate);
router.all('/email-template/edit', EmailTemplateController.editTemplate);
/*  */

/*Logo-manager routing start here*/
router.all('/logo-manager/add', LogoController.add);
router.get('/logo-manager/get-details', LogoController.getDetails);
router.get('/logo-manager/delete/:id', LogoController.delete);
router.get('/logo-manager/view/:id', LogoController.view);
router.all('/logo-manager/edit', LogoController.edit);
router.get('/logo-manager/update-status/:id/:status', LogoController.updateStatus);
/*Logo-manager routing finish here*/
router.get('/settings/game-settings',CommonController.gameSetting);
router.post('/settings/save-game-settings',CommonController.saveGameSetting);


router.get('/game-videos',CommonController.gameVideos);
router.post('/game-videos/add',CommonController.addGameVideos);
router.get('/game-videos/delete/:user_id', CommonController.deleteGameVideo);
router.get('/game-videos/update-status/:id/:status', CommonController.updateStatusGameVideo);
router.get('/game-videos/view/:id', CommonController.viewGameVideo);
router.all('/game-videos/edit', CommonController.editGameVideo);


router.get('/games/get-details',gameController.gameDetails);
router.post('/games/add',gameController.addGame);
router.get('/games/update-status/:id/:status', gameController.updateStatusGame);
router.get('/games/view/:id', gameController.viewGame);
router.all('/games/edit', gameController.editGame);


/*Content-manager routing start here*/
router.get('/content-manager/get-details', ContentManagerController.getDetails);
router.all('/content-manager/add', ContentManagerController.add);
router.all('/content-manager/edit', ContentManagerController.edit);
router.get('/content-manager/delete/:id', ContentManagerController.delete);
router.get('/content-manager/view/:id', ContentManagerController.view);
/*Content-manager routing finish here*/

/*Contractor-manager routing start here*/
router.get('/contractor-manager/get-details', ContractorManagerController.getDetails);
router.all('/contractor-manager/add', ContractorManagerController.add);
router.all('/contractor-manager/edit', ContractorManagerController.edit);
router.get('/contractor-manager/delete/:id', ContractorManagerController.delete);
router.get('/contractor-manager/view/:id', ContractorManagerController.view);
router.get('/contractor-manager/update-status/:id/:status', ContractorManagerController.updateStatus);
/*Contractor-manager routing finish here*/

/*Building-manager routing start here*/
router.get('/building-manager/get-details', BuildingController.getDetails);
router.all('/building-manager/add', BuildingController.add);
router.all('/building-manager/edit', BuildingController.edit);
router.get('/building-manager/delete/:id', BuildingController.delete);
router.get('/building-manager/view/:id', BuildingController.view);
router.get('/building-manager/update-status/:id/:status', BuildingController.updateStatus);
/*Building-manager routing finish here*/

/*inspector-messenger routing start here*/
router.get('/inspector-messenger-manager/get-details', InspectorController.getDetails);
router.all('/inspector-messenger-manager/add', InspectorController.add);
router.all('/inspector-messenger-manager/edit', InspectorController.edit);
router.get('/inspector-messenger-manager/delete/:id', InspectorController.delete);
router.get('/inspector-messenger-manager/view/:id', InspectorController.view);
router.get('/inspector-messenger-manager/update-status/:id/:status', InspectorController.updateStatus);
/*inspector-messenger routing finish here*/

/*Question-manager routing start here*/
router.get('/question-manager/get-details', QuestionController.getDetails);
router.all('/question-manager/add', QuestionController.add);
router.all('/question-manager/edit', QuestionController.edit);
router.get('/question-manager/delete/:id', QuestionController.delete);
router.get('/question-manager/view/:id', QuestionController.view);
router.get('/question-manager/update-status/:id/:status', QuestionController.updateStatus);
/*Question-manager routing finish here*/

/*Answer-manager routing start here*/
router.get('/answer-manager/get-details', AnswerController.getDetails);
router.all('/answer-manager/add', AnswerController.add);
router.all('/answer-manager/edit', AnswerController.edit);
router.get('/answer-manager/delete/:id', AnswerController.delete);
router.get('/answer-manager/view/:id', AnswerController.view);
/*Answer-manager routing finish here*/

/*Answer-manager routing start here*/
router.get('/sponsers', sponserController.getDetails);
router.all('/sponser/add', sponserController.add);
router.all('/sponser/edit', sponserController.edit);
router.get('/sponser/delete/:id', sponserController.delete);
router.get('/sponser/view/:id', sponserController.view);
router.get('/sponser/update-status/:id/:status', sponserController.updateStatus);
/*Answer-manager routing finish here*/

router.all('/email-template/add', EmailTemplateController.addTemplate);
router.get('/email-template/get-details', EmailTemplateController.getTemplateDetails);
router.get('/email-template/view/:id', EmailTemplateController.viewTemplate);
router.all('/email-template/edit', EmailTemplateController.editTemplate);

module.exports = router;
