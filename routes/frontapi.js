var express = require('express');
var router = express.Router();
const usersController = require('../controllers/frontapi/User.Controller');
const commonController = require('../controllers/frontapi/CommonController');
const questionController = require('../controllers/frontapi/Question.Controller');

const gameStageController = require('../controllers/frontapi/GameStages.Controller');


router.post('/register',usersController.saveUser);
router.post('/login',usersController.login);
router.post('/forget-password',usersController.forgetPassword);
router.post('/reset-password',usersController.resetPassword);
router.post('/verify-token',usersController.ValidPasswordToken);
router.get('/users/getuser/:user_id', usersController.getUser);
router.all('/users/updateUser', usersController.updateUser);
router.all('/users/changepassword', usersController.changePassword);
/*Ankit routing start here*/

router.get('/logo-details',commonController.getLogoDetails);
router.get('/game-videos/:video_type',commonController.gameVideos);
router.get('/get-chat/:user_team_id',commonController.getUserChats);
router.get('/pages/:slug',commonController.showCms);
router.all('/team-list',gameStageController.teamList);
router.post('/get-leaderboard',commonController.getLeaderboard);
/*Ankit routing finish here*/

//get stage intro details 
router.post('/get-stage-detail',gameStageController.getStageIntro);
//get team list for particular team id
router.post('/get-team-list',gameStageController.getTeamList);
// for get stage list to assign leader for particular stages
router.post('/get-stage-list',gameStageController.getStageList);
router.post('/assign-leader',gameStageController.assignStageLeader);
router.post('/members-count',gameStageController.teamMemberCount);

router.post('/invite-link',usersController.sendInviteLink);

router.get('/logo-details',commonController.getLogoDetails);
router.get('/game-videos/:video_type',commonController.gameVideos);
router.get('/get-chat/:user_team_id',commonController.getUserChats);
router.get('/game-name/:slug', commonController.getContentName);
/*Ankit routing finish here*/

router.post('/get-stage-detail',gameStageController.getStageIntro);
router.post('/get-stage-research',gameStageController.getStageResearchContent);
router.post('/save-workforce',gameStageController.saveWorkforce);
router.post('/get-workforce',gameStageController.getWorkforce);
router.get('/get-contractors',gameStageController.getContractors);
router.post('/get-contractor',gameStageController.getContractor);
router.get('/game-detail',gameStageController.getGameDetail);

router.post('/save-score',gameStageController.savedScore);
router.post('/team-stage-status',gameStageController.getTeamStageStatus);
router.post('/save-stage-status',gameStageController.saveTeamStageStatus);
router.post('/get-score-details',gameStageController.getStageDetails);

router.post('/get-team-list',gameStageController.getTeamList);
router.post('/get-complete-stage',gameStageController.getCompletedStage);
router.all('/team-list',gameStageController.teamList);
router.get('/pages/:slug',commonController.showCms);


router.get('/update-timer/:time/:user_id',commonController.updateTimer);
router.get('/check-game-timer/:user_id',commonController.checkGameTimer);
router.get('/placement/get-placement-details',commonController.getPlacementDetails);
router.get('/get-placement-category-list',commonController.getPlacementCategoryList);
router.post('/get-placements',commonController.getSelectedPlacement);
router.post('/save-placement',commonController.saveSelectedPlacement);
/*finish*/


/*    */
router.post('/get-stage-issue',questionController.getIssue)
router.post('/save-issue-result',questionController.saveIssueResult)
router.post('/get-issue-result',questionController.getIssueResult)

router.get('/getGameManagerSetting/:slug',commonController.getGameManagerSetting);
router.post('/sponser',commonController.getSponsers);

module.exports = router;


