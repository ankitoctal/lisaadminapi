const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const flash = require("express-flash-messages");
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const expressValidator = require('express-validator');
const fileUpload = require('express-fileupload');
const cors = require('cors');
var app = express();
var fs = require('fs');
var multer = require('multer');
var util = require('util');
const chatRouter = require("./routes/chatroute");
app.use(fileUpload());
app.use(cookieParser());
var session_options = { dir: './sessions' };
app.use(session({
    //store: new FileStore,
    maxAge: 60000,
    secret: 'keyboard',
    resave: false,
    saveUninitialized: true,
}));
app.use(cors());
app.use(bodyParser.json());
//routes
app.use("/chats", chatRouter);
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));
global.base_dir = __dirname;
global.abs_path = function(path) {
    return base_dir + path;
}
app.locals.basedir = path.join(__dirname);
require('./configs/db.js');
require('./libs/common');
var apiRouter = require('./routes/adminapi');
var frontApiRouter = require('./routes/frontapi');
app.use('/adminapi', apiRouter);
app.use('/front/api', frontApiRouter);
//app.listen(4006); // for server
var server = app.listen(4006); 
/*socekt functionality start here*/
// app.use(function(req, res, next) {

//   res.header("Access-Control-Allow-Origin", ['http://localhost:4300','http://localhost:4200']);
//   //<--you can change this with a specific url like http://localhost:4200
//   res.header("Access-Control-Allow-Credentials", true);
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//   res.header("Access-Control-Allow-Headers",'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization');
//   next();
// });
//var io = require('socket.io')(server);
//integrating socketio
var io  = require('socket.io').listen(server);

//database connection
const Chat = require("./models/Chat");
const connect = require("./dbconnect");

//setup event listener
io.on("connection", socket => {
  console.log("user connected");
  socket.on("disconnect", function() {
    console.log("user disconnected");
  });
  //when soemone stops typing
  socket.on("stopTyping", () => {
    io.broadcast.emit("notifyStopTyping");
  });
  socket.on("message", function(msg) { 
    io.emit('message', msg);
      //save chat to the database
      connect.then(db => { 
        console.log(msg)
    	  console.log("connected correctly to the server");
    		let chatMessage = new Chat({ message: msg.message, sender: msg.user_id,user_name:msg.user_name,team_id:msg.team_id});
    		chatMessage.save();
      });
  });
  socket.on('typing', (data) => { console("typing working");
	    socket.broadcast.emit('typing', {data: data, isTyping: true});
	});
});
/*socekt functionality start here*/
